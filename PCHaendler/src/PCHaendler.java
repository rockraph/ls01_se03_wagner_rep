import java.util.Scanner;

public class PCHaendler {

	//private static Scanner myScanner;
	public static void main(String[] args) {
		
		// Benutzereingaben lesen
		
		String artikel = liesString("Was möchten Sie bestellen?");
		int anzahl = Integer.parseInt(liesString("Wie viel möchten Sie bestellen?"));
		double preis = Double.parseDouble(liesString("Geben Sie den Nettopreis ein:"));
		double mwst = Double.parseDouble(liesString("Geben Sie den Mehrwertsteuersatz in Prozent ein:"));
		
		
		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
		
	}
}
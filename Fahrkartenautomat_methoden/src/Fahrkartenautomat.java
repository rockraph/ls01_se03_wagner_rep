import java.util.Scanner;


class Fahrkartenautomat
{
    public static void main(String[] args)
    
   {
    		
        double zuZahlenderBetrag = fahrkartenbestellungerfassen(); 		//Phase 1 - Fahrkartenbestllung erfassen
    	double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag); 	//Phase 2 - FahrkartenBezahlen
        fahrkartenAusgeben(); 							 				//Phase 3 - FahrkartenAusgeben
        rueckgeldAusgeben(r�ckgabebetrag); 								//Phase 4 - R�ckgeldgausgabe
    	
    }
    
	static Scanner tastatur = new Scanner(System.in);
   
    //--------------------------------------------------------------
    //Phase 1 - Fahrkartenbestllung erfassen
    
    private static double fahrkartenbestellungerfassen() {
		// TODO Auto-generated method stub
    	   double zuZahlenderBetrag;
    	   
    	   int anzahl;
    	   System.out.print("Zu zahlender Betrag (EURO): ");
           zuZahlenderBetrag = tastatur.nextDouble();
           
           System.out.print("Wie viele Tickets?: ");
           anzahl = tastatur.nextInt(); 

    	   zuZahlenderBetrag = zuZahlenderBetrag * anzahl;
           
    	   return zuZahlenderBetrag;
    	   
	}
    //--------------------------------------------------------------
    //Phase 2 - FahrkartenBezahlen
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) { 
    	double r�ckgabebetrag;
    	double eingeworfeneM�nze;
    	double eingezahlterGesamtbetrag;
    	eingezahlterGesamtbetrag = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", zuZahlenderBetrag - eingezahlterGesamtbetrag, " Euro");
     	   // System.out.println("Anzahl der Tickets: " +anzahl);
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;

        }	
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
        return r�ckgabebetrag;
    }
 
    //--------------------------------------------------------------
    //Phase 3 - FahrkartenAusgeben
    private static void fahrkartenAusgeben() {
       	System.out.printf("Fahrschein wird ausgegeben\n"); // Phase 2 - Schleifenausgabe
           for (int i = 0; i < 8; i++)
           {
              System.out.print("=");
              try {
    			Thread.sleep(250);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
           }
           System.out.println("\n\n");	
	}
    //--------------------------------------------------------------
    //Phase 4 - R�ckgeldgausgabe
       private static void rueckgeldAusgeben(double r�ckgabebetrag) {
		// TODO Auto-generated method stub
    	   if(r�ckgabebetrag > 0.0)
           {
        	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
        	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

               while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
               {
            	  System.out.println("2.00 EURO");
    	          r�ckgabebetrag -= 2.0;
               }
               while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
               {
            	  System.out.println("1.00 EURO");
    	          r�ckgabebetrag -= 1.0;
               }
               while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
               {
            	  System.out.println("50 CENT");
    	          r�ckgabebetrag -= 0.5;
               }
               while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
               {
            	  System.out.println("20 CENT");
     	          r�ckgabebetrag -= 0.2;
               }
               while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
               {
            	  System.out.println("10 CENT");
    	          r�ckgabebetrag -= 0.1;
               }
               while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
               {
            	  System.out.println("5 CENT");
     	          r�ckgabebetrag -= 0.05;
               }
           }

           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                              "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir w�nschen Ihnen eine gute Fahrt.");   
	   
	}
}
